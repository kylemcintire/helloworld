import webapp2
import jinja2
import os
import validate
import codecs
from google.appengine.ext import db

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir),
                               autoescape=True)


class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    @staticmethod
    def render_str(template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))


class MainPage(Handler):
    def get(self):
        self.render("main.html")


class BirthdayPage(Handler):
    def get(self):
        self.render("birthday.html", error="", month="", day="", year="")

    def post(self):
        user_month = self.request.get('month')
        user_day = self.request.get('day')
        user_year = self.request.get('year')

        month = validate.month(user_month)
        day = validate.day(user_day)
        year = validate.year(user_year)

        if not (month and day and year):
            self.render("birthday.html",
                        error='Invalid Date. Try again!',
                        month=user_month, day=user_day, year=user_year)
        else:
            self.redirect("/birthday/thanks")


class ThanksHandler(webapp2.RequestHandler):
    def get(self):
        self.response.out.write('Looks good!')


class Rot13Page(Handler):
    @staticmethod
    def rot13(s):
        enc = codecs.getencoder("rot-13")
        return enc(s)[0]

    def write_html(self, user_rot13=""):
        self.render("rot13.html", user_rot13=user_rot13)

    def get(self):
        self.write_html()

    def post(self):
        user_input = self.request.get('text')
        user_rot13 = self.rot13(user_input)

        self.write_html(user_rot13)


class User(db.Model):
    username = db.StringProperty(required=True)
    password_hash = db.StringProperty(required=True)
    email = db.EmailProperty(required=True)


class SignupPage(Handler):
    def write_html(self,
                   username="",
                   email="",
                   username_error="",
                   password_error="",
                   verify_error="",
                   email_error=""):

        self.render("signup.html",
                    username=username,
                    email=email,
                    username_error=username_error,
                    password_error=password_error,
                    verify_error=verify_error,
                    email_error=email_error)

    def get(self):
        self.write_html()

    def post(self):

        user_username = self.request.get('username')
        username_check = validate.username(user_username)

        user_password = self.request.get('password')
        user_verify = self.request.get('verify')
        password_check = validate.password_match(user_password, user_verify)

        user_email = self.request.get('email')
        email_check = validate.email(user_email)

        if username_check == "" and password_check == "" and email_check == "":
            user_hash = validate.make_pw_hash(user_username, user_password)
            if validate.valid_pw(user_username, user_password, user_hash):
                user_cookie = 'user_id=%(h)s|%(s)s; Path=/' % {'h': user_hash[0], 's': user_hash[1]}
                self.response.headers.add_header('Set-Cookie', user_cookie)
                self.render('welcome.html', username=user_username)
                # self.response.write(user_cookie)
            else:
                self.response.write("false")
        else:
            self.write_html(user_username,
                            user_email,
                            verify_error=password_check,
                            username_error=username_check,
                            email_error=email_check)


class WelcomePage(Handler):
    def get(self):
        self.render('welcome.html')


class ShoppingPage(Handler):
    def get(self):
        items = self.request.get_all("food")
        self.render("shop.html", items=items)


class FizzBuzzHandler(Handler):
    def get(self):
        n = self.request.get('n', 0)
        n = n and int(n)
        self.render('fizzbuzz.html', n=n)


class Art(db.Model):
    title = db.StringProperty(required=True)
    art = db.TextProperty(required=True)
    created = db.DateTimeProperty(auto_now_add=True)


class AsciiHandler(Handler):
    def render_ascii(self, title="", art="", error=""):
        arts = db.GqlQuery("SELECT * from Art ORDER BY created DESC")

        self.render("ascii.html", title=title, art=art, error=error, arts=arts)

    def get(self):
        self.render_ascii()

    def post(self):
        title = self.request.get("title")
        art = self.request.get("art")

        if title and art:
            a = Art(title=title, art=art)
            a.put()

            self.redirect("/ascii")
        else:
            error = "please enter a title and some artwork"
            self.render_ascii(error=error, title=title, art=art)


class Posts(db.Model):
    subject = db.StringProperty(required=True)
    content = db.TextProperty(required=True)
    created = db.DateTimeProperty(auto_now_add=True)


class RenderBlogHandler(Handler):
    def render_blog(self, posts=None):
        self.render("blog.html", posts=posts)

    def render_new_blog_post(self, subject="", content="", error=""):
        self.render("blog-new-post.html", subject=subject, content=content, error=error)

    def render_blog_post(self, post=None):
        if post is not None:
            self.render("blog-post.html", post=post)
        else:
            self.redirect_to('/blog')


class BlogHandler(RenderBlogHandler):
    def get(self):
        posts = db.GqlQuery("SELECT * from Posts ORDER BY created DESC")
        self.render_blog(posts=posts)


class NewBlogPostHandler(RenderBlogHandler):
    def get(self):
        self.render_new_blog_post()

    def post(self):
        subject = self.request.get("subject")
        content = self.request.get("content")

        if subject and content:
            p = Posts(subject=subject, content=content)
            p.put()

            self.redirect("/blog")
        else:
            error = "Please enter a subject and some content"
            self.render_new_blog_post(error=error, subject=subject, content=content)


class PostHandler(RenderBlogHandler):
    def get(self, postID):
        post = Posts.get_by_id(int(postID))
        self.render_blog_post(post=post)


class VisitCookieHandler(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        visits = self.request.cookies.get('visits', '0')
        # Check that visit is an int
        if visits.isdigit():
            visits = int(visits) + 1
        else:
            visits = 0
        self.response.headers.add_header('Set-Cookie', 'visits=%s' % visits)

        self.response.write('Visits = %s' % visits)


application = webapp2.WSGIApplication([('/', MainPage),
                                       ('/birthday', BirthdayPage),
                                       ('/birthday/thanks', ThanksHandler),
                                       ('/rot13', Rot13Page),
                                       ('/signup', SignupPage),
                                       ('/signup/welcome', WelcomePage),
                                       ('/shop', ShoppingPage),
                                       ('/fizzbuzz', FizzBuzzHandler),
                                       ('/ascii', AsciiHandler),
                                       ('/blog', BlogHandler),
                                       ('/blog/newpost', NewBlogPostHandler),
                                       ('/blog/(\d+)', PostHandler),
                                       ('/cookies', VisitCookieHandler)],
                                      debug=True)
