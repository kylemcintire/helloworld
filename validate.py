import cgi
import re
import random
import string
import hashlib

months = ['January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December']


def escape_html(s):
    return cgi.escape(s, quote=True)


def month(user_month):
    if user_month:
        if user_month.upper() in [m.upper() for m in months]:
            return True


def day(user_day):
    if user_day and user_day.isdigit():
        user_day = int(user_day)
        if 0 < user_day <= 31:
            return True


def year(user_year):
    if user_year and user_year.isdigit():
        user_year = int(user_year)
        if 1900 < user_year < 2020:
            return True


def password_match(user_password, user_v_password):
    if user_password and user_v_password:
        if user_password == user_v_password:
            if re.match("^.{3,20}$", user_password):
                return ""
            else:
                return 'Invalid Password'
        else:
            return 'Your passwords do not match'
    else:
        return 'Please enter a password'


def username(user_username):
    if re.match("^[a-zA-Z0-9_-]{3,20}$", user_username):
        return ""
    else:
        return 'Invalid Username'


def email(user_email):
    if user_email:
        if re.match("^[\S]+@[\S]+\.[\S]+$", user_email):
            return ""
        else:
            return 'Invalid email'
    else:
        return ""


def make_salt():
    return ''.join(random.choice(string.letters) for x in xrange(5))


def make_pw_hash(name, pw, salt=None):
    if salt is None:
        salt = make_salt()

    h = hashlib.sha256(name + pw + salt).hexdigest()
    return [h, salt]


def valid_pw(name, pw, h):
    salt = h[1]
    if make_pw_hash(name, pw, salt) == h:
        return True
    else:
        return False